## ----style, echo = FALSE, results = 'asis'-------------------------------
BiocStyle::markdown()

## ----para, echo = FALSE, warning=FALSE,results='hide'--------------------
knitr::opts_chunk$set(dev="png",fig.show="hold",
               fig.width=8,fig.height=4.5,fig.align="center",
               message=FALSE,collapse=TRUE)
set.seed(1)
suppressMessages(require(png))
suppressMessages(require(grid))

## ----Fig1,echo=FALSE,fig.align='center',fig.width=12, fig.cap="Fig.1 . Principle of neighbors dependency"----
img <- readPNG("Figure1.png")
grid.raster(img)

## ----Fig2, echo=FALSE,fig.align='center', fig.wide=TRUE, fig.cap= "Fig.2 . Principle of a differential interaction"----
img <- readPNG("Hic_3D.PNG")
grid.raster(img)

## ----loadlibraries,message=FALSE, warning=FALSE--------------------------
require(FIND)
require(Matrix)
require(mvtnorm)
require(rasterVis)
require(gridExtra)
require(HiTC)
require(edgeR)
require(ggsci)

## ----generateSimulatedData, warning=FALSE,message=TRUE,cache=TRUE,tidy=TRUE----
data(K562_matrix_small)
simRes <- generateSimulation(K562_matrix_small[1:200,1:200],
                             pDiff = 0.01,pUp = 0.6,
                             foldDiff = 3,
                             extreme.dispersion = 0.1, 
                             nrep1=2,nrep2=2)

## ----display_simResContent-----------------------------------------------
simRes

## ----plot_sumulatedHiCs,fig.align='center',tidy=TRUE, fig.width=12,fig.height=12----
p1 <- levelplot(as.matrix(simRes@Hic1[[1]]), par.settings=YlOrRdTheme,main="Cond1 Rep1")
p2 <- levelplot(as.matrix(simRes@Hic1[[2]]), par.settings=YlOrRdTheme,main="Cond1 Rep2")
p3 <- levelplot(as.matrix(simRes@Hic2[[1]]), par.settings=YlOrRdTheme,main="Cond2 Rep1")
p4 <- levelplot(as.matrix(simRes@Hic2[[2]]), par.settings=YlOrRdTheme,main="Cond2 Rep2")
p5 <- levelplot(as.matrix(simRes@trueStates), par.settings= YlOrRdTheme, main="DCIs position")


# Here we use ncol=1 due to the limite width of page, 
# if you have a large screen you can put 2 plots in a column
grid.arrange(p1,p2,p3,p4,p5, ncol=2)

## ----log2FC, fig.align='center',tidy=TRUE, fig.width=10------------------
## Plot the log2 Fold-change between the two conditions
m1 <- simRes@Hic1[[1]] + simRes@Hic1[[2]]
m2 <- simRes@Hic2[[1]] + simRes@Hic2[[2]]
levelplot(as.matrix(log2((m1+0.01)/(m2+0.01))), par.settings = BuRdTheme, main="log2(Cond1/cond2)")


## ----getDiff, tidy=TRUE--------------------------------------------------
DCis <- getDCIs_fromSim(x = simRes,
                        windowSize =  3,
                        alpha =  0.7, 
                        method = "hardCutoff",
                        qvalue = 1e-2,
                        isrOP_qval =FALSE)


## ----Using_getDCIs_fromMat, eval=FALSE, tidy=TRUE------------------------
#  DCis <- getDCIs_fromMat(Hic1 = simRes@Hic1,
#                          Hic2 = simRes@Hic2,
#                          windowSize =  3,
#                          alpha =  0.7,
#                          method = "hardCutof",
#                          qvalue = 1e-2,
#                          isrOP_qval =FALSE)

## ----plotQvals, fig.align="center",tidy=TRUE, fig.width=12,fig.height=12----
p1 <- levelplot(as.matrix(-log10(DCis@qvals)), par.settings= YlOrRdTheme,main="-log10(qvalue) after cutoff")
p2 <- levelplot(as.matrix(DCis@DCIPos), par.settings= YlOrRdTheme,main="Predicted DCIs")
p3 = levelplot(as.matrix(log2((m1+0.01)/(m2+0.01))), par.settings = BuRdTheme, main="log2(Cond1/cond2)")
grid.arrange(p3,p1,p2,ncol=2)

## ----ExplaineQuantileReg, fig.wide=TRUE, fig.height=5,tidy=TRUE,eval=FALSE----
#  
#  smry <- summary(DCis@qvals)
#  smry$x <- -log10(smry$x)
#  smry$dist <- smry$j - smry$i
#  
#  ## remove very low qvals as they are the majority
#  #smry <- subset(smry , x> -log10(pbeta(0.1,10,18-10+1)) )
#  
#  o <- order(smry$dist)
#  
#  smry <- smry[o,]
#  
#  X <- model.matrix(smry$x ~ splines::bs(smry$dist, df=9))
#  
#    smoothScatter(smry$dist, smry$x, xlab = "Distance",ylab="-log10(qvalue)")
#  
#    quants =seq(0.9,0.99,.01)
#    cols = ggsci::pal_d3()(length(quants))
#    for(j in 1:length(quants)){
#      thr  <- quantreg::rq(x ~ splines::bs(dist,df = 9), data = smry,tau = quants[j])
#      thr.fit <- X %*% thr$coef
#      smry$threshold <- thr.fit
#      lines(smry$dist,smry$threshold,col=cols[j],lwd=2)
#    }
#  
#    legend(max(smry$dist)-20, max(smry$x)-20, legend=quants,
#         col=cols, lty=1, cex=0.5)
#  
#  

## ----getDCI_quantreg, tidy=TRUE------------------------------------------
DCis <- getDCIs_fromSim(x =  simRes, windowSize =  3,alpha =  0.7, 
                        method = "quantreg",quantile = 0.99)

## ----compareWithTrueDCI, tidy=TRUE, fig.width=12,fig.height=12-----------
compareWithTrueDCI(DCis@DCIPos,simRes@trueStates)

## ---- eval=FALSE---------------------------------------------------------
#  
#  DCis_par = getDCIs_parallel(Hic1 = simRes@Hic1,
#                         Hic2 = simRes@Hic2,
#                         windowSize = 3,
#                         alpha = 0.7,
#                         chunk_size=100,
#                         method = "quantreg",
#                         quantile = 0.99,
#                         qvalue = 1e-6,
#                         isrOP_qval = F,
#                         nbProcessors = 4,
#                         verbose =  TRUE)

## ---- eval=FALSE---------------------------------------------------------
#  smry_par = summary(DCis_par@qvals)
#  smry_seq = summary(DCis@qvals)
#  colnames(smry_par)[3] = "qval_par"
#  colnames(smry_seq)[3] = "qval_seq"
#  
#  mrg = merge(smry_par, smry_seq, all=TRUE)
#  
#  # a few sites will show some difference, generally very slight at the decimal positions.
#  nrow(subset(mrg, qval_par != qval_seq))

## ----eval=FALSE----------------------------------------------------------
#  151 151 68.372895
#  151 152 51.179648
#  151 153 37.575510
#  151 154 17.432925
#  151 155  8.201022
#  151 156 18.941581

## ----eval=FALSE----------------------------------------------------------
#  chr1      0  20000  1
#  chr1  20000  40000  2
#  chr1  40000  60000  3
#  chr1  60000  80000  4
#  chr1  80000 100000  5
#  chr1 100000 120000  6

## ----eval=FALSE----------------------------------------------------------
#  matrices_lst <- as.list(list.files("HiC_results/",pattern = "*iced.matrix"))
#  binIndices = "MM9_5000_abs.bed"
#  gw_interactions <- loadHicExperiment(matrices_lst,binIndices = binIndices,
#                                       groups =c(1,1,0,0),type = "HicPro",
#                                       genome = "mm9")

## ----eval=FALSE----------------------------------------------------------
#  gw_interactions <- getDCIs_HicExpr(gw_interactions,
#                                     windowSize = 3,
#                                     alpha = 0.7,
#                                     chunk_size=500,
#                                     method = "quantreg",
#                                     quantile = 0.99,qvalue = 1e-6,isrOP_qval = F,
#                                     nbProcessors = 8,verbose =  TRUE)

## ----tidy=TRUE,eval=FALSE------------------------------------------------
#  # We need to  know the chromosomes lengths
#  require(BSgenome.Hsapiens.UCSC.hg19)
#  
#  seqlen = seqlengths(BSgenome.Hsapiens.UCSC.hg19)[1:22]
#  
#  # We load two K562 replicates and GM12878
#  hic_mats = list(K562_reps1 = "GSM1551620_HIC071_30.hic",
#                 K562_reps2 = "GSM1551623_HIC074_30.hic",
#                 GM12878_reps1 = "GSM1551574_HIC025_30.hic",
#                 GM12878_reps2 = "GSM1551575_HIC026_30.hic")
#  

## ----eval=FALSE----------------------------------------------------------
#  
#  ## We use this function as a proxy to call
#  fct = function(chrom,seqlen, hicfile, resolution){
#  
#    intdata = readJuiceBoxFormat(hicfile,
#                                 chromosome=chrom,
#                                 normMethod="VC_SQRT",
#                                 resolution=resolution)
#  
#    smry <- summary(intdata)
#    bins <- tileGenome(seqlen[chrom],tilewidth=resolution, cut.last.tile.in.chrom=TRUE)
#    bins$name <- 1:length(bins)
#  
#    # correct size
#    intdata <- sparseMatrix(i=smry$i,j=smry$j,x=smry$x, dims=c(length(bins),length(bins)))
#  
#    HTCexp(intdata, bins, bins)
#    }
#  
#  
#  ## Read the list of .hic matrices
#  hic_matrices = list()
#  for(hic in names(hic_mats)){
#  
#    # here we are using chromosom 22, just to test
#    chroms =names(seqlen)[22]
#  
#    hic_lst  = mclapply(chroms,fct,
#    hicfile= hic_mats[[hic]],
#    resolution=5000,
#    seqlen=seqlen,mc.cores=4
#    )
#  
#    hic_matrices[[hic]] = HTClist(hic_lst)
#  }
#  
#  ## We can use the MA-plot technique to do a joint normalization between replicates
#  
#  
#  
#  # define experiment design
#  groups=c(0,0,1,1)
#  
#  # Create a HicExperiment object
#  hicCol <- new("HicExperiment",
#                  hic_matrices= hic_matrices,
#                  groups=groups,
#                  DCIs=list(),
#                  genome="hg19")
#  

## ---- eval=FALSE---------------------------------------------------------
#  # lunch parallel processing.
#  hicCol <- getDCIs_HicExpr(HicExpr=hicCol,
#                                     windowSize = 3,
#                                     alpha = 0.7,
#                                     chunk_size=500,
#                                     method = "quantreg",
#                                     quantile = 0.99,
#                                     qvalue = 1e-6,
#                                     isrOP_qval = F,
#                                     nbProcessors = 8,
#                                     verbose =  TRUE)
#  

## ----sessioninfo---------------------------------------------------------
sessionInfo()

