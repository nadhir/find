# Introduction #

Improvements to the genome-wide chromatin conformation analysis techniques
enabled scientists explore the chromatin structure at an unprecedent scale. 
labs from different backgrounds are starting to adopt the high-resolution Hi-C protocols 
in their studies, which led to an increasing amount of available Hi-C data. 

With more data available, scientists would like perform comparative analysis
between the available Hi-C samples, 
i.e: finding **differential chromatin interactions (DCI)**.

Previously developed differential chromatin interaction detection techniques 
were designed with a low-resolution Hi-C in mind (40kb or more). Thus, 
they based their models on the assumption of the independence between 
the different interacting pairs (Shown in **Fig.1a**), which is logical 
as the low-resolution Hi-C maps capture interactions between relatively 
distant chromatin fibers.

However, when dealing with high-resolution Hi-C (less than 10kb or so) the 
independence assumption between the adjacent "sequares" in the Hi-C matrix does
not hold, due to the polymer nature of the chromatin that imply the dependence 
between adjacent chromatin fibers (**Fig.1b**).

To overcome this issue, we developed a new differential chromatin interaction 
detection technique that takes into consideration the dependency between 
neighboring loci. As depicted in this **Fig.1b**, when there is a change 
in the interaction frequency between the bins $(i,j)$ we expect this movement 
to be reflected on the interaction frequencies in the bins around it

![Fig.1](https://preview.ibb.co/nDW2An/Figure1.png)


### How to install

Download the package from the [download section](https://bitbucket.org/nadhir/find/downloads/).

Before installation, the following dependencies should be installed:
 
```{R}
requiredPackages <- c("Rcpp","RcppEigen","Matrix","bigmemory","HiTC","zlibbioc","devtools",
                      "imager","dplyr","png","quantreg","doParallel","data.table")

install.packages("BiocManager")

missingPackages = setdiff(requiredPackages, .packages(all.available = TRUE))
if(length(missingPackages)>0){
    message(paste0(length(missingPackages), "Package(s) are missing. Installing them ..."))
    BiocManager::install(missingPackages,update=FALSE)
}
```

If you want to install from source you can first clone the repository 

```{r}
git clone https://nadhir@bitbucket.org/nadhir/find.git
```
in R then, you can install it as follows:

```{r}
devtools::install_local("find",force = TRUE,build = TRUE, type = "source")
```

You can also download the already built version [FIND_1.0.3.tar.gz](https://bitbucket.org/nadhir/find/downloads/FIND_1.0.3.tar.gz)

    R CMD INSTALL FIND_1.0.3.tar.gz

### Tutorials

Check the package vignette, in R you can type:

    browseVignettes("FIND")

### Who do I talk to? ###

We really apreciate your feedback, 
if you have any issue or suggestion don't hesitate to contact: djek.nad@gmail.com
