#include <R.h>
#include <Rmath.h>
#include <Rcpp.h>
#include <RcppEigen.h>
using namespace Rcpp;
using namespace Eigen;

using Eigen::SparseMatrix;
using Eigen::VectorXd;
using Eigen::MatrixXd;

// From http://stackoverflow.com/questions/17973442
/*template <typename WHAT>
class ListOf : public List {
    public:
      template <typename T>
      ListOf( const T& x) : List(x){}

    WHAT operator[](int i){ return as<WHAT>( ( (List*)this)->operator[]( i) ) ; }
};*/

// [[Rcpp::plugins(cpp11)]]

Eigen::VectorXd estimateLambda(const List Hic,
                               double mu, // The reference point
                               int starti,int startj,
                               int height, int width,
                                Eigen::MatrixXd sum, // (x1-x2)^2 + (y1-y2)^2
                                Eigen::MatrixXd meansSum){


      if(meansSum.rows() != height || meansSum.cols() != width)
        Rcpp::stop("meansSum should have the same size are the intended window");

      int nbZeros = (meansSum.array() == 0).count();

      Eigen::VectorXd lambda = Eigen::VectorXd::Zero(height * width - nbZeros);


      // if the cell doesn't change by it self, then we don't consider it
      int pos1= std::ceil((double)meansSum.rows()/2)-1;
      int pos2= std::ceil((double)meansSum.cols()/2)-1;

      if(meansSum(pos1,pos2) == 0){
      //  Rcout << "The element doesn't change between samples, ignoring it" << std::endl;
        return lambda;
      }


      if( ((double)lambda.size()/(height * width)) <= 1.0/3.0){
        return lambda;
      }

      // if all the regions is zeros or there is only 1 values in the whole window, skip!
      if(nbZeros  == height * width || lambda.size()==1){
        return lambda;
      }


      // For each replicate
      for(int m=0; m < Hic.size(); m++){

        //Get the surrounding window
        Eigen::MatrixXd tmp(height, width) ;
        tmp = ((SparseMatrix<double>)Hic[m]).block(starti, startj, height, width).eval();

        //x-mu
        tmp-= Eigen::MatrixXd::Constant(height, width, mu);

        //(x-mu)^2
        tmp = tmp.cwiseProduct(tmp);

        //(x-mu)^2
        tmp += sum;

        tmp = tmp.cwiseSqrt(); // x_ik

        tmp = tmp.cwiseProduct(tmp).cwiseProduct(tmp); //x_ik^3


        // if there are some empty bins in both replicates
        // then mark them to be removed
        if(nbZeros > 0){
          for(int i=0;i < meansSum.cols(); i++){
            for(int j=0; j < meansSum.rows();j++){
              if(meansSum(j,i) == 0){
                tmp(j,i) = -1;
              }
            }
          }
        }



        Eigen::VectorXd x1(Map<VectorXd>(tmp.array().data(), tmp.cols()*tmp.rows()));
        Eigen::VectorXd x(lambda.size());


        // TODO: This part needs optimization
        int pos =0;
        if(nbZeros == 0) x = x1;
        else{
          for(int i=0;i< x1.size(); i++){
            if(x1(i) != -1) {
              x(pos) = x1(i);
              pos++;
            }
          }
        }

        std::sort(x.data(), x.data() + x.size());

        lambda += x;
      }


      // Estimate lambda
      for(int k=0; k < lambda.size(); k++){
        if(lambda(k) != 0){
          lambda (k) = ( 3 * (Hic.size() * (k+1) -1)) / (4 * M_PI * lambda(k));
        }
      }
      //Rcout << "Lambda=" << std::endl << lambda << std::endl;
      return lambda;
}


Eigen::SparseMatrix<double> calcMean(const List Hic){

  int nrow1 = ((SparseMatrix<double>)Hic[0]).rows();
  int ncol1 = ((SparseMatrix<double>)Hic[0]).cols();

  SparseMatrix<double> meanHic = SparseMatrix<double>(nrow1,ncol1);
  for(int i=0; i < Hic.size(); i++){
    meanHic +=  (SparseMatrix<double>)Hic[i];
  }
  meanHic /= Hic.size();

  return(meanHic);
}

void checkInput( List Hic1, List Hic2){
  if(Hic1.size() == 0 || Hic2.size() == 0){
    Rcpp::stop("Hic1 or Hic2 cannot be empty");
  }

  // Check if the matrices inside each list have the same size
  int nrow1 = Rcpp::as<Eigen::SparseMatrix<double> >(Hic1[0]).rows();
  int ncol1 = Rcpp::as<Eigen::SparseMatrix<double> >(Hic1[0]).cols();

  int nrow2 = ((SparseMatrix<double>)Hic2[0]).rows();
  int ncol2 = ((SparseMatrix<double>)Hic2[0]).cols();

  if(nrow1 != nrow2)
   Rcpp::stop("Hic1 and Hic2 should have the same number of rows");
  if(ncol1 != ncol2)
   Rcpp::stop("Hic1 and Hic2 should have the same number of cols");

  // check if the elements in each list have the same size
  for(int i=1;i < Hic1.size(); i++){
    if( (((SparseMatrix<double>)Hic1[i]).rows() != nrow1) ||
        (((SparseMatrix<double>)Hic1[i]).cols() != ncol1) ){
      Rcpp::stop("Elements in Hic1 should have the same size");
    }
  }

  for(int i=1;i < Hic2.size(); i++){
    if( (((SparseMatrix<double>)Hic2[i]).rows() != nrow2) ||
      (((SparseMatrix<double>)Hic2[i]).cols() != ncol2) ){
      Rcpp::stop("Elements in Hic2 should have the same size");
    }
  }

}



Eigen::VectorXd estimateR(const List Hic1,
                          const List Hic2,
                          double mu, int starti, int startj,
                          int height, int width,
                          Eigen::MatrixXd means
){
  //Rcout << "Entring estimateR" << std::endl;
  // Calculate (x1-x2)^2 + (y1-y2)^2
  MatrixXd sum = MatrixXd::Zero(height,width);
  int midlei =  ceil((float)height/2 -1);
  int midlej =  ceil((float)width/2 -1 ) ;


  // The matrix is column-wise
  for(int m= 0; m < width; m++){
    for(int n = 0; n < height; n++){
      sum(n,m) = (n- midlei) * (n- midlei) + (m - midlej) * (m - midlej);
    }
  }

  Eigen::VectorXd lambda21 = estimateLambda(Hic2,mu,
                                            starti, startj,
                                            height, width,sum,means);
  //Rcout << "length lambda21=" << lambda21.size() << std::endl;
  
  Eigen::VectorXd lambda11 = estimateLambda(Hic1,mu,
                                            starti, startj,height, width,sum, means);
  
  //Rcout << "length lambda11=" << lambda11.size() << std::endl;
  
  //We add a pseudo-count to avoid a division by zero
  Eigen::VectorXd R_kk = Eigen::VectorXd::Zero( lambda21.size() );

  for(int i=0;i < R_kk.size(); i++){
    if((lambda11(i) ==0) & (lambda21(i) ==0)){
      R_kk(i) = 1;
    }else{
      if(lambda11(i) == 0){
      }else{
        R_kk(i) = lambda21(i) /lambda11(i);
      }
    }
  }

  //Rcout << "length R_kk=" << R_kk.size() << std::endl;
  //R_kk = lambda21.cwiseQuotient(lambda11);


  return R_kk;
}



double rOP(Eigen::VectorXd pvals, int rth, int K){


  if(rth > K){
    Rcpp::stop("Internal error: function rOP, rth > K");
  }
  // Sort p-values
  std::sort(pvals.data(), pvals.data() + pvals.size());

  //Rcout << "Sorted pvalues\n" << pvals << std::endl;

  // take the rth pvalue
  double rpval = pvals(rth);

  //Rcout << "The " << rth+1 << "-th pvalue=\t" << rpval << "k=\t" << K << std::endl;
  // calculate the combined pvalue
  //Here the r is zero based
  double rOP_pval = Rf_pbeta(rpval,rth+1, K-rth,1,0);

  //Rcout << "pbeta(" << rth << "," << K-rth+1 << ")=\t" << rOP_pval << std::endl;

  return rOP_pval;
}

//' @export
// [[Rcpp::export]]
Eigen::SparseMatrix<double> getDiff(const List Hic1,
             const List Hic2,
             int window,
             double percent){

  checkInput(Hic1, Hic2);
  int nrow1 = Rcpp::as<Eigen::SparseMatrix<double> >(Hic1[0]).rows();
  int ncol1 = Rcpp::as<Eigen::SparseMatrix<double> >(Hic1[0]).cols();

  Eigen::SparseMatrix<int> toVisit(nrow1, ncol1);
  SparseMatrix<double> meanHic1 = SparseMatrix<double>(nrow1,ncol1);
  SparseMatrix<double> meanHic2 = SparseMatrix<double>(nrow1,ncol1);

  meanHic1 = calcMean(Hic1);
  meanHic2 = calcMean(Hic2);


  //TODO:This should be improved
  //Get the list of interaction that exist in either one of the conditions
  SparseMatrix<double> meansSum = meanHic1 + meanHic2;


  SparseMatrix<double> lstPvals(nrow1,ncol1);

  int starti,startj, endi, endj;
  int width, height;
  int offset = (int)(window/2);
  double mu1, mu2;

  //Iterate only over non-zero positions
  for(int col=0; col < meansSum.outerSize(); ++col){
    startj = std::max(0, col-offset); // (col-offset>0) ? col-offset : 0;
    endj =   std::min(col + offset,  ncol1-1); //? ncol1 : col+offset;
    //Rcout << "col\t" << col << "\toffset\t" << offset << "\tncol1\t" << ncol1 << std::endl;
    width = endj - startj + 1;



    for(SparseMatrix<double>::InnerIterator it(meansSum,col); it; ++it){

      int r = it.row();
      int c = it.col();

      //Rcout << "(" << r << "," << c << ")" << std::endl;

      //Calculate pval for actual point

      starti = std::max(r -offset,0); // ? r -offset : 0;
      endi = std::min(r +offset, nrow1-1); //? nrow1 : r + offset;
      height = endi-starti + 1;


      //Rcout << "starti:" << starti << "\tendi:" << endi << "\theight:" << height << std::endl;
      //Rcout << "startj:" << startj << "\tendj:" << endj << "\twidth:" << width << std::endl;

      // Add the point to visit (neighbors)
      for(int i = starti; i < endi; i++){
        for(int j = startj; j < endj; j++){
          // Only consider the points that are non-zeros before
          // Cannot use meansSum.coeffRef(i,j) changes the value of it
          if(meanHic1.coeffRef(i,j) == 0 && meanHic2.coeffRef(i,j) ==0){
              toVisit.coeffRef(i,j) = 1;
          }
        }
      }

      /**
       * Estimating pvals given mu1 as refrence
       */
      mu1 = meanHic1.coeffRef(r, c);
      mu2 = meanHic2.coeffRef(r,c);

      Eigen::MatrixXd means(height, width);
      means =  meansSum.block(starti,startj, height, width).eval();
      //Rcout << "means" << std::endl << means << std::endl;
      //Rcout << "mu1\t" << mu1 << std::endl;
      //Rcout << "mu2\t" << mu2 << std::endl;
      
      // if(mu1 == 0 && mu2 == 0){
      //   continue;
      // }
      Eigen::VectorXd R_kk1 = estimateR(Hic1, Hic2, mu1,
                                        starti, startj,
                                        height, width,
                                        means);

      /**
       * Estimating pvals given mu2 as refrence
       */
      Eigen::VectorXd R_kk2 = estimateR(Hic1, Hic2, mu2,
                                        starti, startj,
                                        height, width,
                                        means);
      
      // if a zero value is passed during the creation of SparseMatrix 
      // its location will show with the non-zero values
      if(R_kk2.size() == 0 && R_kk1.size()==0){
        continue;
      }
      means.resize(0,0);

      int df1=0;
      int df2=0;
      // Rcout << "rth= " << rth << std::endl;
      float pl=1,pr=1;

      Eigen::VectorXd pvals =  Eigen::VectorXd::Zero(R_kk1.size() + R_kk2.size() );

      lstPvals.coeffRef(r,c) = 1;
      for(int m=0; m < R_kk1.size(); m++){
        df1 = 2 * Hic1.size() * (m+1);
        df2 = 2 * Hic2.size() * (m+1);


          pr = Rf_pf(R_kk1(m),df1,df2,1,0);
          pl = Rf_pf(R_kk1(m),df1,df2,0,0);

          pvals(m) = 2* std::min(pl,pr);

           //Rcout << "pval=(" << R_kk1(m) << "," << df1 << "," << df2 << ")=\t" <<  pvals(m) << std::endl;

        //}
      }// end for m

      //Rcout << "mu1 pvals :\n" << pvals << std::endl;

      for(int m=0; m < R_kk2.size(); m++){
        df1 = 2 * Hic1.size() * (m+1);
        df2 = 2 * Hic2.size() * (m+1);
        // This one can be passed as a paramter (it is always the same value)

        //if(R_kk2(m) >0){

          pr = Rf_pf(R_kk2(m),df1,df2,1,0);
          pl = Rf_pf(R_kk2(m),df1,df2,0,0);


          //Rcout << "R_kk2(" << m <<")=" << R_kk2(m) << std::endl;
          //Rcout << "pr(" << m << ") =Rf_pf(R_kk2(" << m << ")," << df1 << "," << df2 << ",1) =" << pr  << std::endl;
          //Rcout << "pl(" << m << ") =Rf_pf(R_kk2(" << m << ")," << df1 << "," << df2 << ",0)=" << pl << std::endl;

          pvals(R_kk1.size() + m) = 2* std::min(pl,pr);

          //Rcout << "pval=(" << R_kk2(m) << "," << df1 << "," << df2 << ")=\t" <<  pvals(R_kk1.size() + m) << std::endl;
        //}
      }// end for m


      int rth = ceil(percent * (R_kk1.size()+R_kk2.size()))-1;

      // Calculate the combined p-value using the rOP method
      //Rcout << "mu2 pvals :\n" << pvals << std::endl;
      lstPvals.coeffRef(r,c) = rOP(pvals, rth, pvals.size());
      //Rcout << "rOP(pvals)=\t" <<  lstPvals.coeffRef(r,c) << std::endl;

    }// Inner Iterator
  }// Outer Iterator


  //Rcout << "Neighbouring points" << std::endl;
  // Second pass for the neightboring points
  for(int col=0; col < toVisit.outerSize(); ++col){

    startj =std::max(col-offset,0); //? col-offset : 0;
    endj = std::min(col + offset, ncol1-1);// ? ncol1 : col+offset;
    width = endj - startj + 1;

    for(SparseMatrix<int>::InnerIterator it(toVisit,col); it; ++it){

      int r = it.row();
      int c = it.col();

      //Rcout << "(" << r << "," << c << ") [added]" << std::endl;
      //Calculate pval for actual point
      starti = std::max(r -offset ,0); // ? r -offset : 0;
      endi = std::min(r + offset , nrow1-1);// ? nrow1 : r + offset;
      height = endi - starti + 1;

      /**
      * Estimating pvals given mu1 as refrence
      */


      mu1 = meanHic1.coeffRef(r, c);
      Eigen::MatrixXd means(height, width);
      means =  meansSum.block(starti,startj, height, width).eval();

      Eigen::VectorXd R_kk1 = estimateR(Hic1, Hic2, mu1,
                                        starti, startj,
                                        height, width,
                                        means);


      mu2 = meanHic2.coeffRef(r,c);
      Eigen::VectorXd R_kk2 = estimateR(Hic1, Hic2, mu2,
                                        starti, startj,
                                        height, width,
                                        means);
      
      if(R_kk2.size() == 0 && R_kk1.size()==0){
        continue;
      }
      
      means.resize(0,0);


      int df1=0;
      int df2=0;

      //Rcout << "rth= " << rth << std::endl;
      float pl=1,pr=1;

      Eigen::VectorXd pvals =  Eigen::VectorXd::Zero(R_kk1.size() + R_kk2.size() );

      lstPvals.coeffRef(r,c) = 1;


      for(int m=0; m < R_kk1.size(); m++){
        df1 = 2 * Hic1.size() * (m+1);
        df2 = 2 * Hic2.size() * (m+1);

        //if(R_kk1(m) >0){
          pr = Rf_pf(R_kk1(m),df1,df2,1,0);
          pl = Rf_pf(R_kk1(m),df1,df2,0,0);

          // Rcout << "R_kk1(" << m <<")=" << R_kk1(m) << std::endl;
          // Rcout << "pr(" << m << ") =Rf_pf(R_kk1(" << m << ")," << df1 << "," << df2 << ",1) =" << pr  << std::endl;
          // Rcout << "pl(" << m << ") =Rf_pf(R_kk1(" << m << ")," << df1 << "," << df2 << ",0)=" << pl << std::endl;

          pvals(m) = 2* std::min(pl,pr);
          //Rcout << "pval=(" << R_kk1(m) << "," << df1 << "," << df2 << ")=\t" <<  pvals(m) << std::endl;
        //}
      }// end for m

      //Rcout << "mu1 pvals :\n" << pvals << std::endl;
      /**
      * Estimating pvals given mu2 as refrence
      */


      for(int m=0; m < R_kk2.size(); m++){
        df1 = 2 * Hic1.size() * (m+1);
        df2 = 2 * Hic2.size() * (m+1);

        //if(R_kk2(m) >0){
          pr = Rf_pf(R_kk2(m),df1,df2,1,0);
          pl = Rf_pf(R_kk2(m),df1,df2,0,0);


          // Rcout << "R_kk2(" << m <<")=" << R_kk2(m) << std::endl;
          // Rcout << "pr(" << m << ") =Rf_pf(R_kk2(" << m << ")," << df1 << "," << df2 << ",1) =" << pr  << std::endl;
          // Rcout << "pl(" << m << ") =Rf_pf(R_kk2(" << m << ")," << df1 << "," << df2 << ",0)=" << pl << std::endl;

          pvals(R_kk1.size()+m ) = 2* std::min(pl,pr);
          //Rcout << "pval=(" << R_kk2(m) << "," << df1 << "," << df2 << ")=\t" <<  pvals(R_kk1.size()+m ) << std::endl;
        //}
      }// end for m

      int rth = ceil(percent * (R_kk1.size()+R_kk2.size()))-1;

      //Rcout << "mu2 pvals :\n" << pvals << std::endl;
      lstPvals.coeffRef(r,c) = rOP(pvals, rth, pvals.size());
      //Rcout << "rOP(pvals)=\t" <<  lstPvals.coeffRef(r,c) << std::endl;

    }// interIterator
  }// OuterIterator
  
  
  lstPvals.makeCompressed();
  return lstPvals;
}
